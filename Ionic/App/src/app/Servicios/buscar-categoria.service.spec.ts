import { TestBed } from '@angular/core/testing';

import { BuscarCategoriaService } from './buscar-categoria.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {RouterTestingModule} from "@angular/router/testing";
import {Routes, ActivatedRoute} from "@angular/router";

describe('BuscarCategoriaService', () => {
  const routes: Routes = [
    {
      path: 'buscar-categoria/:id',
      loadChildren: () => import('../Componentes/buscar-categoria/buscar-categoria.module').then(m => m.BuscarCategoriaPageModule)
    },
  ];

  let service: BuscarCategoriaService;
  let controller: HttpTestingController;
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule, RouterTestingModule.withRoutes(routes)],
  }));

  it('should be created', () => {
    controller = TestBed.get(HttpTestingController);
    const service: BuscarCategoriaService = TestBed.get(BuscarCategoriaService);
    expect(service).toBeTruthy();
  });

});
