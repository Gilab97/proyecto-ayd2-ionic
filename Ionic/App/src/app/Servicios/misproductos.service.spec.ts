import { TestBed } from '@angular/core/testing';

import { MisproductosService } from './misproductos.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";

describe('MisproductosService', () => {

  let controller: HttpTestingController;
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
  }));

  it('should be created', () => {
    controller = TestBed.get(HttpTestingController);
    const service: MisproductosService = TestBed.get(MisproductosService);
    expect(service).toBeTruthy();
  });
});
