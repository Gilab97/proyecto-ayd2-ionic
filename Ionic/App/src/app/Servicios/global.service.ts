import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  public usuario: number;
  public nombre: string;
  public id: number;
  public producto: number;
  public lleva: any;
  public tipo: number;
  public categoria: any;
  public idCategoria: number;
  public chat: any;
  constructor() { }
}
