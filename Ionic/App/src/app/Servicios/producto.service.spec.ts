import { TestBed } from '@angular/core/testing';

import { ProductoService } from './producto.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";

describe('ProductoService', () => {

  let controller: HttpTestingController;
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
  }));

  it('should be created', () => {
    controller = TestBed.get(HttpTestingController);
    const service: ProductoService = TestBed.get(ProductoService);
    expect(service).toBeTruthy();
  });
});
