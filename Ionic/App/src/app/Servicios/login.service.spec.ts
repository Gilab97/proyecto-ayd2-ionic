import { TestBed } from '@angular/core/testing';

import { LoginService } from './login.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import { AngularFireAuth } from 'angularfire2/auth';
import { Observable } from 'rxjs';
import {GooglePlus} from '@ionic-native/google-plus/ngx';
import {RouterTestingModule} from "@angular/router/testing";
import {Router, Routes} from "@angular/router";


describe('LoginService', () => {

  const routes: Routes = [
    {
      path: 'home/:usuario',
      loadChildren: () => import('../Componentes/home/home.module').then(m => m.HomePageModule)
    },
  ];

  const mockAngularFireAuth: any = {
    auth: jasmine.createSpyObj('auth', {
      signInAnonymously: Promise.reject({
        code: 'auth/operation-not-allowed'
      }),
      // 'signInWithPopup': Promise.reject(),
      // 'signOut': Promise.reject()
    }),
  };

  let controller: HttpTestingController;
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule, RouterTestingModule.withRoutes(routes)],
    providers: [{ provide: AngularFireAuth, useValue: mockAngularFireAuth }, GooglePlus]
  }));

  it('should be created', () => {
    controller = TestBed.get(HttpTestingController);
    const service: LoginService = TestBed.get(LoginService);
    expect(service).toBeTruthy();
  });
});
