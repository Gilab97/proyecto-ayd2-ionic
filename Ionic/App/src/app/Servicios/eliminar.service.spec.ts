import { TestBed } from '@angular/core/testing';

import { EliminarService } from './eliminar.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('EliminarService', () => {

  let controller: HttpTestingController;
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
  }));

  it('should be created', () => {
    controller = TestBed.get(HttpTestingController);
    const service: EliminarService = TestBed.get(EliminarService);
    expect(service).toBeTruthy();
  });
});
