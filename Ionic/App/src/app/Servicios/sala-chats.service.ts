import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Sala} from "../Modelos/Sala";
import {Mensaje} from "../Modelos/Mensaje";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};
@Injectable({
  providedIn: 'root'
})



export class SalaChatsService {
  private API = 'http://34.71.79.162:3000/';
  //private API = 'http://localhost:3000/';

  constructor(private http: HttpClient) { }

  getUsuarios() {
    return this.http.get(`${this.API}api/user/list-user`);
  }

  getSalaChats(idUsuario) {
    return this.http.get(`${this.API}api/user/give-chat/${idUsuario}`);
  }

  crearSala(request: Sala) {
      return this.http.post(`${this.API}api/sala/createSala`, request, httpOptions);
  }

  mandarMensaje(request: Mensaje) {
    return this.http.post(`${this.API}api/sala/enviarMensaje`, request, httpOptions);
  }
}
