import { TestBed } from '@angular/core/testing';

import { CrearproductoService } from './crearproducto.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('CrearproductoService', () => {

  let controller: HttpTestingController;
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
  }));

  it('should be created', () => {
    controller = TestBed.get(HttpTestingController);
    const service: CrearproductoService = TestBed.get(CrearproductoService);
    expect(service).toBeTruthy();
  });
});
