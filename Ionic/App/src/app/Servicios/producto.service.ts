import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Aproducto} from "../Modelos/producto";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  private API = 'http://34.71.79.162:3000/';
  // private API = 'http://localhost:3000/';

  constructor(private http: HttpClient) { }

  postarticulo(request: Aproducto, idusuario, idproducto){
    return this.http.post(`${this.API}api/product/update/${idusuario}/${idproducto}`, request, httpOptions);
  }

  getproducto(producto) {
    return this.http.get(`${this.API}api/product/listaProductoID/${producto}`);
  }

  eliminar(idproducto){
    return this.http.put(`${this.API}api/product/deleteProducto/${idproducto}`,null, httpOptions);
  }

  getproductosall(){
    return this.http.get(`${this.API}api/product/listproduct`);
  }

  getproductosbusqueda(buscar){
    return this.http.get(`${this.API}api/product/busquedaGlobal/${buscar}`);
  }

  getProductoCategoria(categoria){
    return this.http.get(`${this.API}api/product/busqueda/categoria/${categoria}`);
  }

}
