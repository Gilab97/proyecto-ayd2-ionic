import { TestBed } from '@angular/core/testing';

import { SalaChatsService } from './sala-chats.service';

describe('SalaChatsService', () => {
  let service: SalaChatsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SalaChatsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
