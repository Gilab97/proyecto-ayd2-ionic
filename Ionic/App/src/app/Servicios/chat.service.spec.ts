import { TestBed } from '@angular/core/testing';

import { ChatService } from './chat.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('ChatService', () => {

  let controller: HttpTestingController;
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
  }));

  it('should be created', () => {
    controller = TestBed.get(HttpTestingController);
    const service: ChatService = TestBed.get(ChatService);
    expect(service).toBeTruthy();
  });
});
