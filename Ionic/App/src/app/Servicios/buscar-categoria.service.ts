import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})

export class BuscarCategoriaService {

  private API = 'http://34.71.79.162:3000/';

  constructor(private http: HttpClient) {
  }

  public ObtenerCategorias() {
    return this.http.get(`${this.API}api/product/listaCategorias`);
  }
}
