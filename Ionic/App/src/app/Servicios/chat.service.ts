import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {chat} from "../Modelos/chat";

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  private API = 'http://34.71.79.162:3000/';

  constructor(private http: HttpClient) { }

  postchat(request: chat){
    return this.http.post(`${this.API}`, request, httpOptions);
  }


}
