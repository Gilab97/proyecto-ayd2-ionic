import { TestBed } from '@angular/core/testing';

import { GlobalService } from './global.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";

describe('GlobalService', () => {

  let controller: HttpTestingController;
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
  }));

  it('should be created', () => {
    controller = TestBed.get(HttpTestingController);
    const service: GlobalService = TestBed.get(GlobalService);
    expect(service).toBeTruthy();
  });
});
