import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { regProducto } from "../Modelos/RegistrarProducto";


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})

export class CrearproductoService {
  private API = 'http://34.71.79.162:3000/';

  constructor(private http: HttpClient) {
  }

  public guardar(request: regProducto) {
    return this.http.post(`${this.API}api/product/createProduct`, request, httpOptions);
  }
}
