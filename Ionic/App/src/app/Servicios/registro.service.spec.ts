import { TestBed } from '@angular/core/testing';

import { RegistroService } from './registro.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";

describe('RegistroService', () => {

  let controller: HttpTestingController;
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
  }));

  it('should be created', () => {
    controller = TestBed.get(HttpTestingController);
    const service: RegistroService = TestBed.get(RegistroService);
    expect(service).toBeTruthy();
  });
});
