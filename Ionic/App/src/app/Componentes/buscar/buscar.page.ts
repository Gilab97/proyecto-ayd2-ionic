import { Component, OnInit } from '@angular/core';
import {GlobalService} from '../../Servicios/global.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductoService} from "../../Servicios/producto.service";

@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.page.html',
  styleUrls: ['./buscar.page.scss'],
})
export class BuscarPage implements OnInit {
  buscar: string
  listaproductos: any

  constructor(private activatedRoute: ActivatedRoute,
              public router: Router,
              public global: GlobalService,
              private productomod: ProductoService) { }

  ngOnInit() {
    if (this.global.usuario === null || undefined){
      this.router.navigate([`/`]);
    }
    this.buscar = this.activatedRoute.snapshot.paramMap.get('buscar');

    this.productomod.getproductosbusqueda(this.buscar).subscribe(
        res =>{
          console.log(res);
          this.listaproductos = res
        },error => console.error(error)
    );
  }

  holas(p){
    // alert("Salta a ver toda la info y opcion de compra");
    console.log(p)
    this.global.lleva = p
    this.router.navigate([`/compras/${this.global.id}/${p.idProducto}`]);
  }

}
