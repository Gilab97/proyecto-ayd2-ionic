import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RegistroPage } from './registro.page';
import {Routes} from "@angular/router";
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {FormsModule} from "@angular/forms";
import {LoginComponent} from "../login/login.component";

describe('RegistroPage', () => {
  let component: RegistroPage;
  let fixture: ComponentFixture<RegistroPage>;

  const routes: Routes = [
    {
      path: 'login',
      component: LoginComponent
    },
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistroPage, LoginComponent ],
      imports: [IonicModule.forRoot(), RouterTestingModule.withRoutes(routes), HttpClientTestingModule, FormsModule]
    }).compileComponents();

    fixture = TestBed.createComponent(RegistroPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('simulate alert', () => {
    it('should show alert', fakeAsync(() => {
      component.presentAlert('asdf', 'asdf')
      tick(1000);
      expect(true).toBeTruthy();
    }));
  });

  describe('modify', () => {
    it('should modify', fakeAsync(() => {
      component.nombres1 = "asdf"
      component.apellidos1 = ""
      component.fecha_nacimiento1 = "adfs"
      component.correo_electronico1 = "asdf"
      component.Registrar()
      tick(50);
      expect(component.correo_electronico1).toBeDefined();
    }));
  });

  describe('simulate alert', () => {
    it('should show alert', fakeAsync(() => {
      component.returnLogin()
      tick(1000);
      expect(component.router.navigated).toBeTruthy();
    }));
  });
});
