import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PerfilproductoPage } from './perfilproducto.page';

const routes: Routes = [
  {
    path: '',
    component: PerfilproductoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PerfilproductoPageRoutingModule {}
