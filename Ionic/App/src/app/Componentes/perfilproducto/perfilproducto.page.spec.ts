import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PerfilproductoPage } from './perfilproducto.page';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {FormsModule} from "@angular/forms";
import {Router, Routes} from "@angular/router";
import {RouterTestingModule} from "@angular/router/testing";

describe('PerfilproductoPage', () => {
  let component: PerfilproductoPage;
  let fixture: ComponentFixture<PerfilproductoPage>;

  const routes: Routes = [
    {path: 'home/:usuario', loadChildren: () => import('../home/home.module').then(m => m.HomePageModule)},
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerfilproductoPage ],
      imports: [IonicModule.forRoot(), RouterTestingModule.withRoutes(routes), HttpClientTestingModule, FormsModule]
    }).compileComponents();

    fixture = TestBed.createComponent(PerfilproductoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('user input', () => {
    it('should define', fakeAsync(() => {
      component.setinfo([{Nombre: '', descripcion: '', precio: '', imagen: ''}])
      tick(50);
      expect(component.nombres1).toBeDefined();
    }));
  });

  describe('modify', () => {
    it('should modify', fakeAsync(() => {
      component.setinfo([{Nombre: '', descripcion: '', precio: '', imagen: ''}])
      component.Modificar()
      tick(50);
      expect(component.nombres1).toBeDefined();
    }));
  });

  describe('delete', () => {
    it('should delete', fakeAsync(() => {
      component.Eliminar()
      tick(50);
      expect(true).toBeTruthy();
    }));
  });

  describe('close image', () => {
    it('should close', fakeAsync(() => {
      component.closeImage()
      tick(25);
      expect(true).toBeTruthy();
    }));
  });

  describe('delay', () => {
    it('should pause', fakeAsync(() => {
      component.pausa()
      tick(2000);
      expect(true).toBeTruthy();
    }));
  });

  describe('change listener', () => {
    it('should change', fakeAsync(() => {
      const byteCharacters = atob('iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==')
      const byteNumbers = new Array(byteCharacters.length);
      for (let i = 0; i < byteCharacters.length; i++) {
        byteNumbers[i] = byteCharacters.charCodeAt(i);
      }
      const byteArray = new Uint8Array(byteNumbers);
      const blob = new Blob([byteArray], {type: 'image/png'});
      component.changeListener({target: {files: [blob]}})
      tick(25);
      expect(true).toBeTruthy();
    }));
  });

  describe('change image', () => {
    it('should change', fakeAsync(() => {
      component.chargeImage('iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==','png')
      tick(25);
      expect(true).toBeTruthy();
    }));
  });
});
