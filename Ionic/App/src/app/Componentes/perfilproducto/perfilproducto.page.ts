import { Component, OnInit } from '@angular/core';
import {Aproducto, Mproducto} from "../../Modelos/producto";
import {GlobalService} from "../../Servicios/global.service";
import {ProductoService} from "../../Servicios/producto.service";
import {Router, ActivatedRoute} from "@angular/router";


@Component({
  selector: 'app-perfilproducto',
  templateUrl: './perfilproducto.page.html',
  styleUrls: ['./perfilproducto.page.scss'],
})
export class PerfilproductoPage implements OnInit {
  nombres1: string;
  apellidos1: string;
  precio1: string;
  imagen: string;
  base64: string;
  extension: string;

  file;
  name: string;
  showSelector = true;

  request: Aproducto;

  newproduct(nombre, apellido, precio, base1, ext) : Mproducto{
    return {
      precio: precio,
      Nombre: nombre,
      descripcion: apellido,
      base64: base1,
      extension: ext
    }
  };

  constructor(private productomod: ProductoService,
              private global : GlobalService,
              public router : Router) { }

  ngOnInit() {
    this.base64 = '';
    this.productomod.getproducto(this.global.producto).subscribe(
        res =>{
          this.setinfo(res);
          console.log(res);
        },error => console.error(error)
    );
  }

  Modificar(){
    this.request = this.newproduct(this.nombres1, this.apellidos1, this.precio1, this.base64, this.extension);
    console.log(this.request);
    this.productomod.postarticulo(this.request, this.global.id, this.global.producto).subscribe(
        res =>{
          console.log(res);
          alert('Producto Modificado');
          this.returnHome();
        },
        error => console.error(error)
    )
  }

  returnHome(){
    this.router.navigate([`/home/${this.global.id}`]);
  }

  Eliminar(){
    console.log(this.global.producto);
    this.productomod.eliminar(this.global.producto).subscribe(
        res =>{
          alert('Producto Eliminado');
          this.returnHome()
        },
        error => console.error(error)
    )
  }

  setinfo(res){
    this.nombres1 = res[0].Nombre;
    this.apellidos1 = res[0].descripcion;
    this.precio1 = res[0].precio;
    this.imagen = res[0].imagen;
  }

  pausa(){
    setTimeout(() => {
        duration: 200; // The length in milliseconds the animation should take.
    },2000);
  }

  changeListener($event): void {
    this.showSelector = false
    this.file = $event.target.files[0];
    this.getFile(this.file);
  }

  getFile(newFile){
    const reader = new FileReader();
    reader.readAsDataURL(newFile);
    reader.onload = (e) => {
      this.name = newFile.name.split('.')[0];
      this.base64 = reader.result as string;
      this.base64 = this.base64.split(',')[1];
      this.extension = newFile.name.split('.')[1];
      this.chargeImage(this.base64, this.extension)
    }
  }

  chargeImage(base64, extension){
    const image = document.getElementById('image') as HTMLImageElement;
    if(image) image.src = `data:image/${extension};base64, ${base64}`
  }

  closeImage(){
    this.file = ''
    this.name = ''
    this.showSelector = true
  }
}
