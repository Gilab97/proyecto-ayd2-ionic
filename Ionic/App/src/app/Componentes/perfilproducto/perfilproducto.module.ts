import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PerfilproductoPageRoutingModule } from './perfilproducto-routing.module';

import { PerfilproductoPage } from './perfilproducto.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PerfilproductoPageRoutingModule
  ],
  declarations: [PerfilproductoPage]
})
export class PerfilproductoPageModule {}
