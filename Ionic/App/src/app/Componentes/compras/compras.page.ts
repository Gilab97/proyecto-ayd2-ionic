import { Component, OnInit } from '@angular/core';
import {GlobalService} from "../../Servicios/global.service";
import {Router, ActivatedRoute} from "@angular/router";
import {chat} from "../../Modelos/chat";
import {ChatService} from "../../Servicios/chat.service";

@Component({
  selector: 'app-compras',
  templateUrl: './compras.page.html',
  styleUrls: ['./compras.page.scss'],
})
export class ComprasPage implements OnInit {

  imagen = ""
  nombre = ""
  precio = ""
  descripcion = ""
  datos = ""
  ubicacion = ""

  request: chat

  newchat(usuario1, usuario2, mensaje): chat{
    return {
      id: usuario1,
      id2: usuario2,
      mensaje
    }
  }

  constructor(public global: GlobalService,
              public Chatservices: ChatService) { }

  ngOnInit() {
    if (this.global.lleva) { this.setinfo(this.global.lleva) }
  }

  Comprar(){
    alert("enviar notificaciones de compra")
    this.request = this.newchat(this.global.id, this.global.lleva.idUsuario, "Quiero adquirir este producto")
    this.Chatservices.postchat(this.request).subscribe(
        res => {
          console.log(res)
        },
        error => console.error(error)
    )
  }

  Preguntar(){
    alert("enviar notificaciones de pregunta")
    this.request = this.newchat(this.global.id, this.global.lleva.idUsuario, "¿Se encuentra disponible?")
    this.Chatservices.postchat(this.request).subscribe(
        res => {
          console.log(res)
        },
        error => console.error(error)
    )
  }

  setinfo(p){
    if (p.imagen === "" || p.imagen === null || p.imagen === undefined){
      this.imagen = "https://i.pinimg.com/originals/c5/12/e1/c512e1cba1b6867fca63d6889b69fde3.png"
    }
    else{
      this.imagen = p.imagen
    }
    if (p.nombre === undefined){
      this.nombre = p.Nombre
    }
    else {
      this.nombre = p.nombre
    }
    this.precio = p.precio
    this.descripcion = p.descripcion
    this.datos = `Publicado por: ${p.nombres} ${p.apellidos}`;
    this.ubicacion = p.nombre_residencial
  }

}
