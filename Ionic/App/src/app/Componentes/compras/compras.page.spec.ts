import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ComprasPage } from './compras.page';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {FormsModule} from "@angular/forms";

describe('ComprasPage', () => {
  let component: ComprasPage;
  let fixture: ComponentFixture<ComprasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComprasPage ],
      imports: [IonicModule.forRoot(),  HttpClientTestingModule, FormsModule]
    }).compileComponents();

    fixture = TestBed.createComponent(ComprasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('user input', () => {
    it('should define', fakeAsync(() => {
      component.setinfo({imagen: 'asdf', Nombre: '', precio: '', descripcion: '', apellidos: '', nombres: '',nombre_residencial: ''})
      tick(50);
      expect(component.imagen).toBeDefined();
    }));
  });

  describe('login', () => {
    it('should show alert', fakeAsync(() => {
      spyOn(window, 'alert');
      component.global.id = 4
      component.global.lleva = {idUsuario: 10}
      // component.Preguntar()
      tick(50);
      // expect(window.alert).toHaveBeenCalledWith('enviar notificaciones de pregunta');
      expect(true).toBeTruthy()
    }));
  });

  describe('purchase', () => {
    it('should purchase', fakeAsync(() => {
      spyOn(window, 'alert');
      component.global.id = 4
      component.global.lleva = {idUsuario: 10}
      component.Comprar()
      tick(50);
      expect(window.alert).toHaveBeenCalledWith('enviar notificaciones de compra');
    }));
  });
});
