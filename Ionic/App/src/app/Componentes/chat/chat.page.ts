import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {GlobalService} from "../../Servicios/global.service";
import {SalaChatsService} from "../../Servicios/sala-chats.service";
import {Mensaje} from "../../Modelos/Mensaje";

@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {
  idUsuario1: number
  idUsuario2: number
  id_chat: number
  mensajes: any = []
  enviar: string
  objeto: any
  emisor: number

  constructor(private activatedRoute: ActivatedRoute,
              public router: Router,
              public global: GlobalService,
              private chats: SalaChatsService) { }

  ngOnInit() {
    this.mensajes = []
    this.chats.getSalaChats(this.global.id).subscribe(
        res =>{
          for (var i in res) {
            if(res[i].id1 == this.global.usuario || res[i].id2 == this.global.usuario){
              if(res[i].id2 == this.global.chat.Usuario || res[i].id1 == this.global.chat.Usuario){
                this.id_chat = res[i].idSalaChat
                if(this.global.usuario == res[i].id1){
                  this.emisor = 1
                }
                else{
                  this.emisor = 0
                }
                break
              }
            }
          }



          for (var i in res) {
            var objeto = {};
            if(res[i].idSalaChat == this.id_chat)
            {
              if(res[i].Usuario1 == 0)
              {
                  objeto = ({
                    "Usuario": res[i].Nombre2,
                    "Mensaje": res[i].Mensaje,
                  });
              }else {
                objeto = ({
                  "Usuario": res[i].Nombre1,
                  "Mensaje": res[i].Mensaje,
                });
              }
              this.mensajes.push(objeto)
            }
            }
          console.log(this.mensajes)
          },error => console.error(error)
    );

  }

  Regresar(){
    this.router.navigate([`/sala-chats/${this.global.id}/`]);
  }

  Mandar(){
    this.objeto = {
      'idSalaChat': this.id_chat,
      'mensaje': this.enviar,
      'user1': this.emisor
    }
    this.chats.mandarMensaje(this.objeto).subscribe(
        res => {
          console.log(res);
          this.ngOnInit();
        },
        error => console.error(error)
    )
  }
}
