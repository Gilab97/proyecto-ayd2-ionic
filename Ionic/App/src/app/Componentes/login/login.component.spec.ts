import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LoginComponent } from './login.component';
import {Router, Routes} from "@angular/router";
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {FormsModule} from "@angular/forms";
import {AngularFireAuth} from "angularfire2/auth";
import {GooglePlus} from "@ionic-native/google-plus/ngx";
import {LoginService} from "../../Servicios/login.service";

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let service: LoginService

  const routes : Routes = [
    {path: 'compras/:iduser/:idpro', loadChildren: () => import('../home/home.module').then(m => m.HomePageModule)},
    {path: 'registro', loadChildren: () => import('../registro/registro.module').then( m => m.RegistroPageModule)},
  ]

  const mockAngularFireAuth: any = {
    auth: jasmine.createSpyObj('auth', {
      signInAnonymously: Promise.reject({
        code: 'auth/operation-not-allowed'
      }),
      // 'signInWithPopup': Promise.reject(),
      // 'signOut': Promise.reject()
    }),
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponent ],
      imports: [IonicModule.forRoot(), RouterTestingModule.withRoutes(routes), HttpClientTestingModule, FormsModule],
      providers: [{ provide: AngularFireAuth, useValue: mockAngularFireAuth }, GooglePlus]
    }).compileComponents();

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    service = TestBed.get(LoginService)
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('user input', () => {
    it('should define', fakeAsync(() => {
      component.Userinput({target: {value: 'asdf'}})
      tick(50);
      expect(component.user).toBeDefined();
    }));
  });

  describe('password input', () => {
    it('should define', fakeAsync(() => {
      component.Passwordinput({target: {value: 'asdf'}})
      tick(50);
      expect(component.password).toBeDefined();
    }));
  });

  describe('login', () => {
    it('should show alert', fakeAsync(() => {
      spyOn(window, 'alert');
      component.Loginnormal()
      tick(50);
      expect(window.alert).toHaveBeenCalledWith('Usuario o contraseña vacios');
    }));
  });

  describe('login', () => {
    it('should show alert', fakeAsync(() => {
      spyOn(window, 'alert');
      component.user = 2123
      component.password = "adsf"
      component.Loginnormal()
      tick(50);
      expect(true).toBeTruthy();
    }));
  });

  describe('login', () => {
    it('should show alert', fakeAsync(() => {
      spyOn(window, 'alert');
      component.logingoogle()
      tick(50);
      expect(window.alert).toHaveBeenCalledWith('los datos son incorrectos');
    }));
  });
});
