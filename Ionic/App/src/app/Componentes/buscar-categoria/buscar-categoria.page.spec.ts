import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BuscarCategoriaPage } from './buscar-categoria.page';
import {Routes} from "@angular/router";
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {FormsModule} from "@angular/forms";

describe('BuscarCategoriaPage', () => {
  let component: BuscarCategoriaPage;
  let fixture: ComponentFixture<BuscarCategoriaPage>;

  const routes: Routes = [
    {
      path: 'home/:usuario',
      loadChildren: () => import('../home/home.module').then(m => m.HomePageModule)
    },
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuscarCategoriaPage ],
      imports: [IonicModule.forRoot(), RouterTestingModule.withRoutes(routes), HttpClientTestingModule, FormsModule]
    }).compileComponents();

    fixture = TestBed.createComponent(BuscarCategoriaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
