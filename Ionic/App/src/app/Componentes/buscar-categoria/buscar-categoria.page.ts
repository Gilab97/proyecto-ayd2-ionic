import { Component, OnInit } from '@angular/core';
import {GlobalService} from '../../Servicios/global.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductoService} from "../../Servicios/producto.service";

@Component({
  selector: 'app-buscar-categoria',
  templateUrl: './buscar-categoria.page.html',
  styleUrls: ['./buscar-categoria.page.scss'],
})
export class BuscarCategoriaPage implements OnInit {
  categoria: any
  listaproductos: any
  idCategoria: any

  constructor(private activatedRoute: ActivatedRoute,
              public router: Router,
              public global: GlobalService,
              private productomod: ProductoService) { }

  ngOnInit() {
    if (this.global.usuario === null || undefined){
      this.router.navigate([`/`]);
    }
    this.categoria = this.global.categoria;
    this.idCategoria = this.global.idCategoria;

    console.log(this.idCategoria);

    this.productomod.getProductoCategoria(this.idCategoria).subscribe(
        res =>{
          console.log(res);
          this.listaproductos = res
        },error => console.error(error)
    );
  }

  holas(p){
    // alert("Salta a ver toda la info y opcion de compra");
    console.log(p)
    this.global.lleva = p
    this.router.navigate([`/compras/${this.global.id}/${p.idProducto}`]);
  }

}
