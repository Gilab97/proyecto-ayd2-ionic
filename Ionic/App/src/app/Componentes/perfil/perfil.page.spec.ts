import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PerfilPage } from './perfil.page';
import {Routes} from "@angular/router";
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {FormsModule} from "@angular/forms";

describe('PerfilPage', () => {
  let component: PerfilPage;
  let fixture: ComponentFixture<PerfilPage>;

  const routes: Routes = [
    {
      path: 'home/:usuario',
      loadChildren: () => import('../home/home.module').then(m => m.HomePageModule)
    },
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PerfilPage ],
      imports: [IonicModule.forRoot(), RouterTestingModule.withRoutes(routes), HttpClientTestingModule, FormsModule]
    }).compileComponents();

    fixture = TestBed.createComponent(PerfilPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('simulate alert', () => {
    it('should show alert', fakeAsync(() => {
      component.global.usuario = null
      component.presentAlert('asdf', 'asdf')
      tick(1000);
      expect(true).toBeTruthy();
    }));
  });

  describe('modify', () => {
    it('should modify', fakeAsync(() => {
      component.nombres1 = "asdf"
      component.apellidos1 = ""
      component.fecha_nacimiento1 = "adfs"
      component.correo_electronico1 = "asdf"
      component.Modificar()
      tick(50);
      expect(component.correo_electronico1).toBeDefined();
    }));
  });

  describe('user input', () => {
    it('should define', fakeAsync(() => {
      component.setInfo({nombres: 'asdf', apellidos: '', idUsuario: '', contrasena: '', fecha_nacimiento: ''})
      tick(50);
      expect(component.nombres1).toBeDefined();
    }));
  });
});
