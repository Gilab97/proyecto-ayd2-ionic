import { Component, OnInit } from '@angular/core';
import {GlobalService} from '../../Servicios/global.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductoService} from "../../Servicios/producto.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  public user;
  listaproductos: any

  constructor(private activatedRoute: ActivatedRoute,
              public router: Router,
              public global: GlobalService,
              private productomod: ProductoService) { }

  ngOnInit() {
    if (this.global.usuario === null){
      this.router.navigate([`/`]);
    }
    this.global.id = +this.activatedRoute.snapshot.paramMap.get('usuario');
    console.log(this.global.usuario);
    this.user = this.global.nombre;

    this.productomod.getproductosall().subscribe(
        res =>{
          console.log(res);
          this.listaproductos = res
        },error => console.error(error)
    );
  }

  holas(p){
    // alert("Salta a ver toda la info y opcion de compra");
    console.log(p)
    this.global.lleva = p
    this.router.navigate([`/compras/${this.global.id}/${p.idProducto}`]);
  }
}
