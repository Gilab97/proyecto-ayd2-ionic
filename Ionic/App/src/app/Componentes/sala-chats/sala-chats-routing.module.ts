import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SalaChatsPage } from './sala-chats.page';

const routes: Routes = [
  {
    path: '',
    component: SalaChatsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SalaChatsPageRoutingModule {}
