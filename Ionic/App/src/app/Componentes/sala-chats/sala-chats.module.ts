import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SalaChatsPageRoutingModule } from './sala-chats-routing.module';

import { SalaChatsPage } from './sala-chats.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SalaChatsPageRoutingModule
  ],
  declarations: [SalaChatsPage]
})
export class SalaChatsPageModule {}
