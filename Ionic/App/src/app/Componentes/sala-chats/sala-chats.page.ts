import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {GlobalService} from "../../Servicios/global.service";
import {SalaChatsService} from "../../Servicios/sala-chats.service";
import {Sala} from "../../Modelos/Sala";

@Component({
  selector: 'app-sala-chats',
  templateUrl: './sala-chats.page.html',
  styleUrls: ['./sala-chats.page.scss'],
})
export class SalaChatsPage implements OnInit {
  idUsuario1: number;
  idUsuario2: number;
  ultimoid: number;
  usuarios: any;
  request: Sala;
  salaschats: any = []

  newChat(): Sala{
        return{
            Usuario1: this.global.usuario,
            Usuario2: this.idUsuario2,
        }
    };

  constructor(private activatedRoute: ActivatedRoute,
              public router: Router,
              public global: GlobalService,
              private chats: SalaChatsService) { }

  ngOnInit() {
    this.chats.getUsuarios().subscribe(
        res =>{
          console.log(res);
          this.usuarios = res
        },error => console.error(error)
    );

    this.chats.getSalaChats(this.global.id).subscribe(

        res =>{
            this.salaschats = []
          for (var i in res) {
            this.ultimoid = res[i].idSalaChat
          }

          for (var j = 1; j <= this.ultimoid; j++) {
            var objeto = {};

            for (var i in res) {
              if(res[i].idSalaChat == j){
                  if(res[i].id1 == this.global.usuario) {
                      objeto = ({
                          "Usuario": res[i].id2,
                          "Nombre": res[i].Nombre2,
                          "Mensaje": res[i].Mensaje
                      });
                      this.salaschats.push(objeto)
                  }else{
                      objeto = ({
                          "Usuario": res[i].id1,
                          "Nombre": res[i].Nombre1,
                          "Mensaje": res[i].Mensaje
                      });
                      this.salaschats.push(objeto)
                  }
                  }
            }

          }
        },error => console.error(error)
    );

  }

    CrearChats(){
        console.log(this.global.usuario)
        console.log(this.idUsuario2)
        var enviar = ({
           'Usuario1':this.global.usuario,
           'Usuario2':this.idUsuario2
        });
        this.chats.crearSala(enviar).subscribe(
            res => {
                this.global.chat = {
                    "Usuario": this.idUsuario2,
                    "Nombre": "",
                    "Mensaje": ""
                }
                this.router.navigate([`/chat/${this.global.id}/${this.idUsuario2}`]);
            },
            error => console.error(error)
        )
    }

    verChats(chat){
        this.global.chat = chat;
        this.router.navigate([`/chat/${this.global.id}/${chat.Usuario}`]);
    }


}
