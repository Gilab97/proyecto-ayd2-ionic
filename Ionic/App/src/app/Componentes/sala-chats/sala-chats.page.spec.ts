import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SalaChatsPage } from './sala-chats.page';

describe('SalaChatsPage', () => {
  let component: SalaChatsPage;
  let fixture: ComponentFixture<SalaChatsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalaChatsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SalaChatsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
