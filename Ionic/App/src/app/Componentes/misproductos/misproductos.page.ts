import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {GlobalService} from "../../Servicios/global.service";
import { MisproductosService } from "../../Servicios/misproductos.service";

@Component({
  selector: 'app-misproductos',
  templateUrl: './misproductos.page.html',
  styleUrls: ['./misproductos.page.scss'],
})
export class MisproductosPage implements OnInit {
  listaproductos: any;
  idUser: number;

  constructor(private activatedRoute: ActivatedRoute,
              public router: Router,
              public global: GlobalService,
              private miProductoService: MisproductosService ) { }

  ngOnInit() {
    this.idUser = this.global.id;
    this.miProductoService.ObtenerProductos(this.idUser).subscribe(
        res => {
          console.log(res);
          this.listaproductos = res; },
          error => console.error(error)
    );
  }

  holas(p){
    this.global.lleva = p;
    this.global.producto = p.idProducto;
    this.router.navigate([`/perfilproducto/${this.global.id}/${p.idProducto}`]);
  }

}
