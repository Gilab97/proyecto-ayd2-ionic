import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MisproductosPage } from './misproductos.page';
import {Routes} from "@angular/router";
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {FormsModule} from "@angular/forms";

describe('MisproductosPage', () => {
  let component: MisproductosPage;
  let fixture: ComponentFixture<MisproductosPage>;

  const routes: Routes = [
    {
      path: 'perfilproducto/:iduser/:idpro',
      loadChildren: () => import('../perfilproducto/perfilproducto.module').then(m => m.PerfilproductoPageModule)
    },
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MisproductosPage ],
      imports: [IonicModule.forRoot(), RouterTestingModule.withRoutes(routes), HttpClientTestingModule, FormsModule]
    }).compileComponents();

    fixture = TestBed.createComponent(MisproductosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
