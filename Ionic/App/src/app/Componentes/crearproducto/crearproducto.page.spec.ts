import {async, ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CrearproductoPage } from './crearproducto.page';
import {RouterTestingModule} from "@angular/router/testing";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {FormsModule} from "@angular/forms";
import {Routes} from "@angular/router";

describe('CrearproductoPage', () => {
  let component: CrearproductoPage;
  let fixture: ComponentFixture<CrearproductoPage>;

  const routes: Routes = [
    {
      path: 'compras/:iduser/:idpro',
      loadChildren: () => import('../compras/compras.module').then(m => m.ComprasPageModule)
    },
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearproductoPage ],
      imports: [IonicModule.forRoot(), RouterTestingModule.withRoutes(routes), HttpClientTestingModule, FormsModule]
    }).compileComponents();

    fixture = TestBed.createComponent(CrearproductoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('change listener', () => {
    it('should change', fakeAsync(() => {
      const byteCharacters = atob('iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==')
      const byteNumbers = new Array(byteCharacters.length);
      for (let i = 0; i < byteCharacters.length; i++) {
        byteNumbers[i] = byteCharacters.charCodeAt(i);
      }
      const byteArray = new Uint8Array(byteNumbers);
      const blob = new Blob([byteArray], {type: 'image/png'});
      component.changeListener({target: {files: [blob]}})
      tick(25);
      expect(true).toBeTruthy();
    }));
  });

  describe('change image', () => {
    it('should change', fakeAsync(() => {
      component.chargeImage('iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==','png')
      tick(25);
      expect(true).toBeTruthy();
    }));
  });

  describe('close image', () => {
    it('should close', fakeAsync(() => {
      component.closeImage()
      tick(25);
      expect(true).toBeTruthy();
    }));
  });

  describe('modify', () => {
    it('should modify', fakeAsync(() => {
      component.nombre = "asdf"
      component.base64 = ""
      component.extension = "adfs"
      component.descripcion = "asdf"
      component.Guardar()
      tick(50);
      expect(component.nombre).toBeDefined();
    }));
  });

  describe('simulate alert', () => {
    it('should show alert', fakeAsync(() => {
      // component.presentAlert('asdf', 'asdf')
      tick(1000);
      expect(true).toBeTruthy();
    }));
  });
});
