import { Component, OnInit } from '@angular/core';
import { regProducto } from "../../Modelos/RegistrarProducto";
import {CrearproductoService} from "../../Servicios/crearproducto.service";
import {ActivatedRoute, Router} from "@angular/router";
import {AlertController} from "@ionic/angular";
import {GlobalService} from "../../Servicios/global.service";

@Component({
  selector: 'app-crearproducto',
  templateUrl: './crearproducto.page.html',
  styleUrls: ['./crearproducto.page.scss'],
})
export class CrearproductoPage implements OnInit {

  nombre: string; descripcion: string; precio:number; base64: string; extension: string;
  idUser: number;
  file;
  name: string;
  showSelector = true;
  request: regProducto;


  newProducto(nomb, descrip, prec, user, base, ext): regProducto{
  return {
    Nombre: nomb,
    Descripcion: descrip,
    Precio: prec,
    idUsuario: user,
    base64: base,
    extension: ext
    }
  };

  constructor(private activatedRoute: ActivatedRoute, private crearService: CrearproductoService,
              public router : Router, private global : GlobalService, public alertController: AlertController) { }

  ngOnInit() {
    this.base64 = '';
  }

  Guardar(){
    this.idUser = this.global.id;
    this.request = this.newProducto(this.nombre, this.descripcion, this.precio, this.idUser, this.base64, this.extension);
    console.log(this.request);
    this.crearService.guardar(this.request).subscribe(
        res => {
          console.log(res);
          this.presentAlert('Felicidades', 'Producto agregado');
          this.returnHome();
        },
        error => console.error(error)
    )
  }

  async presentAlert(title, message) {
    const alert = await this.alertController.create({
      header: title,
      message: message,
      buttons: ['OK']
    });

    await alert.present();
  }

  returnHome(){
    this.router.navigate([`/home/${this.idUser}`]);
  }


  changeListener($event): void {
    this.showSelector = false
    this.file = $event.target.files[0];
    this.getFile(this.file);
  }

  getFile(newFile){
    const reader = new FileReader();
    reader.readAsDataURL(newFile);
    reader.onload = (e) => {
      this.name = newFile.name.split('.')[0];
      this.base64 = reader.result as string;
      this.base64 = this.base64.split(',')[1];
      this.extension = newFile.name.split('.')[1];
      this.chargeImage(this.base64, this.extension)
    }
  }

  chargeImage(base64, extension){
    const image = document.getElementById('image') as HTMLImageElement;
    if(image) image.src = `data:image/${extension};base64, ${base64}`
  }

  closeImage(){
    this.file = ''
    this.name = ''
    this.showSelector = true
  }
}
