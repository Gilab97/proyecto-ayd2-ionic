import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import {LoginComponent} from './Componentes/login/login.component'

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  },
  {
    path: 'home/:usuario',
    loadChildren: () => import('./Componentes/home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'registro',
    loadChildren: () => import('./Componentes/registro/registro.module').then( m => m.RegistroPageModule)
  },
  {
    path: 'perfil/:usuario',
    loadChildren: () => import('./Componentes/perfil/perfil.module').then( m => m.PerfilPageModule)
  },
  {
    path: 'perfilproducto/:iduser/:idpro',
    loadChildren: () => import('./Componentes/perfilproducto/perfilproducto.module').then(m => m.PerfilproductoPageModule)
  },
  {
    path: 'buscar/:iduser/:buscar',
    loadChildren: () => import('./Componentes/buscar/buscar.module').then(m => m.BuscarPageModule)
  },
  {
    path: 'compras/:iduser/:idpro',
    loadChildren: () => import('./Componentes/compras/compras.module').then(m => m.ComprasPageModule)
  },
  {
    path: 'crearproducto/:iduser',
    loadChildren: () => import('./Componentes/crearproducto/crearproducto.module').then( m => m.CrearproductoPageModule)
  },
  {
    path: 'misproductos/:iduser',
    loadChildren: () => import('./Componentes/misproductos/misproductos.module').then(m => m.MisproductosPageModule)
  },
  {
    path: 'buscar-categoria/:id',
    loadChildren: () => import('./Componentes/buscar-categoria/buscar-categoria.module').then(m => m.BuscarCategoriaPageModule)
  },
  {
    path: 'sala-chats/:id',
    loadChildren: () => import('./Componentes/sala-chats/sala-chats.module').then( m => m.SalaChatsPageModule)
  },
  {
    path: 'chat/:id1/:id2',
    loadChildren: () => import('./Componentes/chat/chat.module').then( m => m.ChatPageModule)
  },










];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
