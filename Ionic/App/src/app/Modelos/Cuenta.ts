export interface Cuenta{
    idUsuario: number,
    nombres: string,
    apellidos: string,
    fecha_nacimiento: string,
    correo_electronico: string,
    contrasena: string,
    descripcion: string,
    tipo_usuario: number,
    id_colonia: number,
    res: boolean
}
