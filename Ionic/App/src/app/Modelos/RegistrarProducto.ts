export interface regProducto{
    Nombre: string,
    Descripcion: string,
    Precio: number,
    idUsuario: number,
    base64: string,
    extension: string
}
