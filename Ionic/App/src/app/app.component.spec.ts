import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import {TestBed, async, fakeAsync, tick} from '@angular/core/testing';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { RouterTestingModule } from '@angular/router/testing';

import { AppComponent } from './app.component';
import {Routes} from "@angular/router";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {FormsModule} from "@angular/forms";
import {LoginComponent} from "./Componentes/login/login.component";

describe('AppComponent', () => {

  let statusBarSpy, splashScreenSpy, platformReadySpy, platformSpy;

  const routes: Routes = [
    {
      path: 'perfil/:usuario',
      loadChildren: () => import('./Componentes/perfil/perfil.module').then( m => m.PerfilPageModule)
    },
    {
      path: 'home/:usuario',
      loadChildren: () => import('./Componentes/home/home.module').then(m => m.HomePageModule)
    },
    {
      path: 'buscar/:iduser/:buscar',
      loadChildren: () => import('./Componentes/buscar/buscar.module').then(m => m.BuscarPageModule)
    },
    {
      path: 'crearproducto/:iduser',
      loadChildren: () => import('./Componentes/crearproducto/crearproducto.module').then( m => m.CrearproductoPageModule)
    },
    {
      path: 'misproductos/:iduser',
      loadChildren: () => import('./Componentes/misproductos/misproductos.module').then(m => m.MisproductosPageModule)
    },
    {
      path: '',
      component: LoginComponent
    },
  ];

  beforeEach(async(() => {
    statusBarSpy = jasmine.createSpyObj('StatusBar', ['styleDefault']);
    splashScreenSpy = jasmine.createSpyObj('SplashScreen', ['hide']);
    platformReadySpy = Promise.resolve();
    platformSpy = jasmine.createSpyObj('Platform', { ready: platformReadySpy });

    TestBed.configureTestingModule({
      declarations: [AppComponent, LoginComponent],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        { provide: StatusBar, useValue: statusBarSpy },
        { provide: SplashScreen, useValue: splashScreenSpy },
        { provide: Platform, useValue: platformSpy },
      ],
      imports: [ RouterTestingModule.withRoutes(routes), HttpClientTestingModule, FormsModule],
    }).compileComponents();
  }));

  it('should create the app', async () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should initialize the app', async () => {
    TestBed.createComponent(AppComponent);
    expect(platformSpy.ready).toHaveBeenCalled();
    await platformReadySpy;
    expect(statusBarSpy.styleDefault).toHaveBeenCalled();
    expect(splashScreenSpy.hide).toHaveBeenCalled();
  });

  describe('define 0', () => {
    it('should define', fakeAsync(() => {
      const fixture = TestBed.createComponent(AppComponent);
      const component = fixture.componentInstance;
      component.salida(0)
      tick(50);
      expect(component.selectedIndex3).toBeDefined();
    }));
  });

  describe('define 1', () => {
    it('should define', fakeAsync(() => {
      const fixture = TestBed.createComponent(AppComponent);
      const component = fixture.componentInstance;
      component.salida(1)
      tick(50);
      expect(component.selectedIndex3).toBeDefined();
    }));
  });

  describe('login', () => {
    it('should redirect', fakeAsync(() => {
      const fixture = TestBed.createComponent(AppComponent);
      const component = fixture.componentInstance;
      component.global.usuario = null
      component.verbuscar()
      tick(50);
      expect(component.router.navigated).toBeTruthy();
    }));
  });

  describe('onInit', () => {
    it('should do', fakeAsync(() => {
      const fixture = TestBed.createComponent(AppComponent);
      const component = fixture.componentInstance;
      component.ngOnInit()
      tick(50);
      expect(true).toBeTruthy();
    }));
  });

});
