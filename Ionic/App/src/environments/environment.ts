// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const firebaseConfig = {
  apiKey: 'AIzaSyBluzpLCe5lAIYdZCyuyLY-Rq0qaEYJWPc',
  authDomain: 'login-650af.firebaseapp.com',
  databaseURL: 'https://login-650af.firebaseio.com',
  projectId: 'login-650af',
  storageBucket: 'login-650af.appspot.com',
  messagingSenderId: '932233909521',
  appId: '1:932233909521:web:cd906f08b3b700bcb528b0'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
